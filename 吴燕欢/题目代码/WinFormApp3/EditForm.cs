﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp3
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }
        private int id;

        public EditForm(int id, string name, int age, int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            //更新
            if (this.id > 0)
            {
                var sql = string.Format("update students set Name = '{0}',age = {1},score = {2} where Id = {3}", name, age, score, this.id);

                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("更新成功", "提示");
            }
            else
            {
                var sql = string.Format("insert into students(Name,Age,Score) values('{0}','{1}','{2}')", name, age, score);

                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("添加成功", "提示");
            }

            this.DialogResult = DialogResult.Yes;

            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
